package rest

import (
	"bytes"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	neturl "net/url"
	"os"
	"strings"
)

type Client struct {
}

func (c *Client) Get(url string, headers map[string]string) (*http.Response, error) {
	// Create GET request.
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	// Set headers.
	for key, value := range headers {
		request.Header.Add(key, value)
	}

	// Do request.
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return response, err
}

func (c *Client) Post(url string, headers map[string]string, body string) (*http.Response, error) {
	// Create POST request.
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBufferString(body))
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	// Set headers.
	for key, value := range headers {
		request.Header.Add(key, value)
	}

	// Do request.
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return response, err
}

func (c *Client) Put(url string, headers map[string]string, body string) (*http.Response, error) {
	// Create PUT request.
	request, err := http.NewRequest(http.MethodPut, url, bytes.NewBufferString(body))
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	// Set headers.
	for key, value := range headers {
		request.Header.Add(key, value)
	}

	// Do request.
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return response, err
}

func (c *Client) Delete(url string, headers map[string]string) (*http.Response, error) {
	// Create DELETE request.
	request, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	// Set headers.
	for key, value := range headers {
		request.Header.Add(key, value)
	}

	// Do request.
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return response, err
}

func (c *Client) PostForm(url string, headers map[string]string, parameters map[string]string) (*http.Response, error) {
	// Set parameters to from body.
	body := neturl.Values{}
	for key, value := range parameters {
		body.Add(key, value)
	}

	// Create POST request.
	request, err := http.NewRequest(http.MethodPost, url, strings.NewReader(body.Encode()))
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	// Set headers.
	for key, value := range headers {
		request.Header.Add(key, value)
	}
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	// Do request.
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return response, err
}

func (c *Client) PostMultipart(uri string, headers map[string]string, parameters map[string]string, formFiles []Multipart) (*http.Response, error) {
	// Create formFile writer.
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// Write form formFiles.
	for _, formFile := range formFiles {
		file, err := os.Open(formFile.FilePath)
		if err != nil {
			return nil, err
		}

		formFile, err := writer.CreateFormFile(formFile.FieldName, formFile.FileName)
		if err != nil {
			return nil, err
		}

		_, err = io.Copy(formFile, file)
		if err != nil {
			return nil, err
		}

		err = file.Close()
		if err != nil {
			return nil, err
		}
	}

	// Write parameters.
	for key, value := range parameters {
		err := writer.WriteField(key, value)
		if err != nil {
			return nil, err
		}
	}

	// Close formFile writer.
	err := writer.Close()
	if err != nil {
		return nil, err
	}

	// Create POST request.
	request, err := http.NewRequest(http.MethodPost, uri, body)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	// Set headers.
	for key, value := range headers {
		request.Header.Add(key, value)
	}
	request.Header.Set("Content-Type", writer.FormDataContentType())

	// Do request.
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return response, err
}
