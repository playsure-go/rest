package rest

type Multipart struct {
	FieldName string
	FileName  string
	FilePath  string
}
